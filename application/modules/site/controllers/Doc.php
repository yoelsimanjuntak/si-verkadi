<?php
class Doc extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    /*$ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('TIDAK MEMILIKI HAK AKSES!');
      exit();
    }*/
  }

  public function index($tipe) {
    $data['title'] = "DOKUMEN ".strtoupper($tipe);
    $data['tipe'] = $tipe;
    $this->template->load('backend', 'site/doc/index', $data);
  }

  public function index_load($tipe) {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterSkpd = !empty($_POST['filterSkpd'])?$_POST['filterSkpd']:null;

    $orderdef = array(COL_SKPDNAMA=>'asc');
    $orderables = array(null,COL_DOCNAME,COL_DOCNOMOR,COL_DOCTAHUN,null,COL_CREATEDON);
    $cols = array(COL_DOCNAME,COL_DOCNOMOR,COL_DOCTAHUN);
    $condSkpd = "1=1";

    if(!empty($filterSkpd)) {
      $condSkpd = "IdSkpd=".$filterSkpd;
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $condSkpd = "IdSkpd=".$ruser[COL_COMPANYID];
      }
    }

    $queryAll = $this->db->where(COL_DOCTYPE, $tipe)->where($condSkpd)->get(TBL_TDOCS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tdocs.*, _userinformation.Name, _userinformation.Name, mskpd.SkpdNama')
    ->where(COL_DOCTYPE, $tipe)
    ->where($condSkpd)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TDOCS.".".COL_CREATEDON,"left")
    ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TDOCS.".".COL_IDSKPD,"left")
    ->order_by(COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TDOCS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        '<a class="btn btn-xs btn-danger btn-action '.($r[COL_DOCSTATUS]=='VERIFIED'?'disabled':'').'" href="'.site_url('site/doc/delete/'.$r[COL_DOCID]).'"><i class="far fa-times-circle"></i></a>&nbsp;'.
        '<a class="btn btn-xs btn-primary" href="'.site_url('site/doc/view/'.$r[COL_DOCID]).'"><i class="far fa-search"></i></a>'
        /*'<a class="btn btn-xs btn-'.($r[COL_SKPDISAKTIF]==1?'warning':'info').' btn-action" href=""><i class="far fa-refresh"></i></a>'*/,
        $r[COL_DOCNAME].($ruser=COL_ROLEID==ROLEADMIN?'<br /><small class="font-italic">'.$r[COL_SKPDNAMA].'</small>':''),
        $r[COL_DOCNOMOR],
        $r[COL_DOCTAHUN],
        '<span class="badge bg-'.($r[COL_DOCSTATUS]=='VERIFIED'?'success':'secondary').'">'.($r[COL_DOCSTATUS]=='VERIFIED'?'VERIFIED':'SUBMITTED').'</span>',
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($tipe) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_DOCTYPE=>$tipe,
        COL_IDSKPD=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_IDSKPD):$ruser[COL_COMPANYID],
        COL_DOCNAME=>$this->input->post(COL_DOCNAME),
        COL_DOCREMARKS=>$this->input->post(COL_DOCREMARKS),
        COL_DOCNOMOR=>$this->input->post(COL_DOCNOMOR),
        COL_DOCTAHUN=>$this->input->post(COL_DOCTAHUN),

        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES)) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }
          $upl = $this->upload->data();
          $dat[COL_DOCURL] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_TDOCS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $docID = $this->db->insert_id();
        $res = $this->db->insert(TBL_TDOCLOGS, array(
          COL_DOCID=>$docID,
          COL_LOGREMARKS=>'Dokumen diterima.',
          COL_LOGFILE=>$dat[COL_DOCURL],
          COL_LOGBY=>$ruser[COL_USERNAME],
          COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('site/doc/form');
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_DOCID, $id)->get(TBL_TDOCS)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_DOCID, $id)->delete(TBL_TDOCS);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      if(!empty($rdata[COL_DOCURL])&&file_exists(MY_UPLOADPATH.$rdata[COL_DOCURL])) {
        unlink(MY_UPLOADPATH.$rdata[COL_DOCURL]);
      }
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function view($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('DATA TIDAK DITEMUKAN');
      exit();
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_COMPANYID]!=$rdata[COL_IDSKPD]) {
      show_error('MAAF, ANDA TIDAK MEMILIKI HAK AKSES');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_DOCNAME=>$this->input->post(COL_DOCNAME),
        COL_DOCREMARKS=>$this->input->post(COL_DOCREMARKS),
        COL_DOCNOMOR=>$this->input->post(COL_DOCNOMOR),
        COL_DOCTAHUN=>$this->input->post(COL_DOCTAHUN),

        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file'])) {
          $config['upload_path'] = MY_UPLOADPATH;
          $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
          $config['max_size']	= 5120;
          $config['max_width']  = 4000;
          $config['max_height']  = 4000;
          $config['overwrite'] = FALSE;

          $this->load->library('upload',$config);

          $res = $this->upload->do_upload('file');
          if($res) {
            $upl = $this->upload->data();
            $dat[COL_DOCURL] = $upl['file_name'];
          }
        }

        $res = $this->db->where(COL_DOCID, $id)->update(TBL_TDOCS, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL_TDOCLOGS, array(
          COL_DOCID=>$id,
          COL_LOGREMARKS=>'Dokumen diperbarui.',
          COL_LOGFILE=>isset($dat[COL_DOCURL])?$dat[COL_DOCURL]:null,
          COL_LOGBY=>$ruser[COL_USERNAME],
          COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      $data['title'] = 'RINCIAN DOKUMEN';
      $data['data'] = $rdata;
      $this->template->load('backend', 'site/doc/detail', $data);
    }
  }

  public function form_log($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      /*$config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;*/
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_DOCID=>$id,
        COL_LOGREMARKS=>$this->input->post(COL_LOGREMARKS),
        COL_LOGBY=>$ruser[COL_USERNAME],
        COL_LOGTIMESTAMP=>date("Y-m-d H:i:s")
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }

          $upl = $this->upload->data();
          $dat[COL_LOGFILE] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_TDOCLOGS, $dat);
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Catatan berhasil ditambahkan.');
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('site/doc/form-log', $data);
    }
  }

  public function change_status($id) {
    $stat = $this->input->post("stat");
    $ruser = GetLoggedUser();

    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      ShowJsonError('Maaf, anda tidak memiliki hak akses!');
      exit();
    }

    $rdata = $this->db
    ->where(COL_DOCID, $id)
    ->get(TBL_TDOCS)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      exit();
    }

    /*if($rdata[COL_DOCSTATUS]==$stat) {
      ShowJsonSuccess('Status dokumen tidak berubah.');
      exit();
    }*/

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_DOCID, $id)->update(TBL_TDOCS, array(
        COL_DOCSTATUS=>$stat,
        COL_ISVERIFKESESUAIAN=>$this->input->post(COL_ISVERIFKESESUAIAN)?1:0,
        COL_ISVERIFKODE=>$this->input->post(COL_ISVERIFKODE)?1:0,
        COL_ISVERIFKELENGKAPAN=>$this->input->post(COL_ISVERIFKELENGKAPAN)?1:0,
        COL_VERIFIEDREMARKS=>$this->input->post(COL_VERIFIEDREMARKS),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->insert(TBL_TDOCLOGS, array(
        COL_DOCID=>$id,
        COL_LOGREMARKS=>'Status diperbarui.',
        COL_LOGBY=>$ruser[COL_USERNAME],
        COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('PEMBARUAN STATUS DOKUMEN BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }
}
