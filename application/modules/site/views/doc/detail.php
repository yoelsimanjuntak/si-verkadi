<?php
$ruser = GetLoggedUser();
$rlog = $this->db
->select("tdoclogs.*, COALESCE(_userinformation.Name, tdoclogs.LogBy) as LogName")
->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TDOCLOGS.".".COL_LOGBY,"left")
->where(TBL_TDOCLOGS.'.'.COL_DOCID, $data[COL_DOCID])
->order_by(COL_LOGTIMESTAMP, 'desc')
->get(TBL_TDOCLOGS)
->result_array();
?>
<style>
.input-group span.select2-selection {
  border-top-right-radius: 0 !important;
  border-bottom-right-radius: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-olive">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">RINCIAN</h6>
          </div>
          <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="form-group">
              <label>SKPD</label>
              <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%;" disabled>
                <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, $data[COL_IDSKPD])?>
              </select>
            </div>
            <div class="form-group">
              <label>NAMA DOKUMEN</label>
              <input type="text" class="form-control" placeholder="Nama / Judul Dokumen" name="<?=COL_DOCNAME?>" value="<?=!empty($data)?$data[COL_DOCNAME]:''?>" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'disabled':''?> />
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-9">
                  <label>NOMOR</label>
                  <input type="text" class="form-control" placeholder="Nomor Dokumen" name="<?=COL_DOCNOMOR?>" value="<?=!empty($data)?$data[COL_DOCNOMOR]:''?>" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'disabled':''?> />
                </div>
                <div class="col-sm-3">
                  <label>TAHUN</label>
                  <input type="number" class="form-control" placeholder="Tahun" name="<?=COL_DOCTAHUN?>" value="<?=!empty($data)?$data[COL_DOCTAHUN]:''?>" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'disabled':''?> />
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>KETERANGAN</label>
              <textarea class="form-control" rows="2" placeholder="Keterangan / Rincian / Catatan Tambahan" name="<?=COL_DOCREMARKS?>" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'disabled':''?>><?=!empty($data)?$data[COL_DOCREMARKS]:''?></textarea>
            </div>
            <div class="form-group">
              <label>LAMPIRAN</label>
              <div id="div-attachment">
                <?php
                if($data[COL_DOCSTATUS]!='VERIFIED') {
                  ?>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
                    </div>
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
                      <label class="custom-file-label" for="file">PILIH FILE</label>
                    </div>
                  </div>
                  <?php
                }
                ?>

                <?php
                $file = $data[COL_DOCURL];
                if(file_exists(MY_UPLOADPATH.$data[COL_DOCURL])) {
                  ?>
                  <ul class="todo-list ui-sortable" data-widget="todo-list">
                    <li>
                      <div class="d-inline mr-2">
                        <i class="far fa-paperclip"></i>
                      </div>
                      <span class="text font-italic"><?=$data[COL_DOCURL]?></span>
                      <div class="tools">
                        <a href="javascript:window.open('<?=MY_UPLOADURL.$data[COL_DOCURL]?>')"><i class="far fa-download"></i></a>
                      </div>
                    </li>
                  </ul>
                  <?php
                }
                ?>
                <p class="mt-2 text-sm text-muted mb-0 font-italic">
                  <strong>CATATAN:</strong><br />
                  <ul class="text-sm text-muted mb-0 font-italic" style="padding-left: 2rem !important">
                    <li>Unggah dokumen akan memperbarui dokumen yang sudah ada sebelumnya.</li>
                    <li>Besar file / dokumen maksimum <strong>5 MB</strong>.</li>
                    <li>Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG / PNG</strong>, <strong>PDF</strong>, <strong>Word</strong>, dan <strong>Excel</strong>.</li>
                  </ul>
                </p>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('site/doc/index/'.strtolower($data[COL_DOCTYPE]))?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'disabled':''?>><i class="far fa-check-circle"></i>&nbsp;PERBARUI</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
      <div class="col-sm-6">
        <?php
        if($ruser[COL_ROLEID]==ROLEADMIN) {
          ?>
          <div class="card card-olive">
            <div class="card-header">
              <h6 class="card-title font-weight-bold">VERIFIKASI</h6>
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="control-label col-sm-3">STATUS</label>
                <div class="col-sm-6">
                  <select class="form-control" name="<?=COL_DOCSTATUS?>">
                    <option value="SUBMITTED" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'':'selected'?>>SUBMITTED</option>
                    <option value="VERIFIED" <?=$data[COL_DOCSTATUS]=='VERIFIED'?'selected':''?>>VERIFIED</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-3">CHECKLIST</label>
                <div class="col-sm-9">
                  <div class="icheck-primary">
                    <input type="checkbox" id="radioVerif1" name="<?=COL_ISVERIFKESESUAIAN?>" <?=$data[COL_ISVERIFKESESUAIAN]?'checked':''?>>
                    <label for="radioVerif1" class="font-weight-normal">Kesesuaian Program / Kegiatan dengan RKPD</label>
                  </div>
                  <div class="icheck-primary">
                    <input type="checkbox" id="radioVerif2" name="<?=COL_ISVERIFKODE?>" <?=$data[COL_ISVERIFKODE]?'checked':''?>>
                    <label for="radioVerif2" class="font-weight-normal">Kesesuaian Kode Rekening dengan Kegiatan</label>
                  </div>
                  <div class="icheck-primary">
                    <input type="checkbox" id="radioVerif3" name="<?=COL_ISVERIFKELENGKAPAN?>" <?=$data[COL_ISVERIFKELENGKAPAN]?'checked':''?>>
                    <label for="radioVerif3" class="font-weight-normal">Kelengkapan Dokumen RKA</label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-3">CATATAN</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_VERIFIEDREMARKS?>"><?=$data[COL_VERIFIEDREMARKS]?></textarea>
                </div>
              </div>
            </div>
            <div class="card-footer text-right">
              <button type="button" id="btn-status" class="btn btn-sm btn-primary"><i class="far fa-sync"></i>&nbsp;UBAH STATUS</button>
            </div>
          </div>
          <?php
        } else {
          ?>
          <div class="card card-olive">
            <div class="card-header">
              <h6 class="card-title font-weight-bold">HASIL VERIFIKASI</h6>
            </div>
            <div class="card-body p-0">
              <table class="table table-bordered">
                <tr>
                  <td>STATUS</td><td style="width: 10px; white-space: nowrap">:</td>
                  <td><?=$data[COL_DOCSTATUS]=='VERIFIED'?'<span class="badge bg-success">TERVERIFIKASI</span>':'<span class="badge bg-secondary">BELUM TERVERIFIKASI</span>'?></td>
                </tr>
                <tr>
                  <td rowspan="3">CHECKLIST</td>
                  <td rowspan="3" style="width: 10px; white-space: nowrap">:</td>
                  <td style="padding-left: 1rem !important"><span class="font-weight-bold pr-2"><?=$data[COL_ISVERIFKESESUAIAN]?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger text-muted"></i>'?></span>&nbsp;<span class="font-italic">Kesesuaian Program / Kegiatan dengan RKPD</span></td>
                </tr>
                <tr>
                  <td style="padding-left: 1rem !important"><span class="font-weight-bold pr-2"><?=$data[COL_ISVERIFKODE]?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger text-muted"></i>'?></span>&nbsp;<span class="font-italic">Kesesuaian Kode Rekening dengan Kegiatan</span></td>
                </tr>
                <tr>
                  <td style="padding-left: 1rem !important"><span class="font-weight-bold pr-2"><?=$data[COL_ISVERIFKELENGKAPAN]?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger text-muted"></i>'?></span>&nbsp;<span class="font-italic">Kelengkapan Dokumen RKA</span></td>
                </tr>
                <tr>
                  <td>CATATAN</td><td style="width: 10px; white-space: nowrap">:</td>
                  <td><?=$data[COL_VERIFIEDREMARKS]?></td>
                </tr>
              </table>
            </div>
          </div>
          <?php
        }
        ?>


        <div class="card card-olive">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">RIWAYAT / CATATAN</h6>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 50px; white-space: nowrap">WAKTU</th>
                  <th>KETERANGAN</th>
                  <th style="width: 50px; white-space: nowrap">OLEH</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rlog)) {
                  foreach($rlog as $r) {
                    $desc_ = $r[COL_LOGREMARKS];
                    $files_ = "";
                    if(!empty($r[COL_LOGFILE])) {
                      $arrfile = explode(",", $r[COL_LOGFILE]);
                      foreach($arrfile as $f) {
                        if(file_exists(MY_UPLOADPATH.$f)) $files_ .= '<a href="'.(MY_UPLOADURL.$f).'" target="_blank"><i class="far fa-link pr-2"></i></a>';
                      }
                    }
                    ?>
                    <tr>
                      <td style="width: 50px; white-space: nowrap"><?=date('d-m-Y', strtotime($r[COL_LOGTIMESTAMP]))?>&nbsp;<sup class="font-italic"><?=date('H:i', strtotime($r[COL_LOGTIMESTAMP]))?></sup></td>
                      <td><?=$desc_?><?=!empty($r[COL_LOGFILE])?'<span class="pull-right">'.$files_.'</span>':''?></td>
                      <td style="max-width:250px; overflow:hidden !important; text-overflow:ellipsis; white-space:nowrap; font-style: italic"><?=$r['LogName']?></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3" class="text-center font-italic text-sm">KOSONG</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <a id="btn-add-log" href="<?=site_url('site/doc/form-log/'.$data[COL_DOCID])?>" class="btn btn-primary btn-sm"><i class="far fa-edit"></i>&nbsp;TAMBAH CATATAN</a>
          </div>
        </div>

    </div>
  </div>
</section>
<div class="modal fade" id="modalLog" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">TAMBAH CATATAN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer d-block">
        <div class="row">
          <div class="col-lg-12 text-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
            <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalLog = $('#modalLog');
$(document).ready(function() {
  bsCustomFileInput.init();
  modalLog.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalLog).empty();
  });

  $('#form-main').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('#btn-add-log').click(function(){
    var url = $(this).attr('href');
    modalLog.modal('show');
    $('.modal-body', modalLog).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalLog).load(url, function(){
      $('button[type=submit]', modalLog).unbind('click').click(function(){
        $('form', modalLog).submit();
      });
    });
    return false;
  });

  $('#btn-status').click(function(){
    if(confirm('Apakah anda yakin ingin memperbarui status dokumen?')) {
      var stat_ = $('[name=DocStatus]').val();
      var verif1 = $('[name=IsVerifKesesuaian]').is(':checked')?1:0;
      var verif2 = $('[name=IsVerifKode]').is(':checked')?1:0;
      var verif3 = $('[name=IsVerifKelengkapan]').is(':checked')?1:0;
      var remarks = $('[name=VerifiedRemarks]').val();
      $.post('<?=site_url('site/doc/change-status/'.$data[COL_DOCID])?>', {
        stat: stat_,
        IsVerifKesesuaian: verif1,
        IsVerifKode: verif2,
        IsVerifKelengkapan: verif3,
        VerifiedRemarks: remarks
      }, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
          setTimeout(function(){
            location.reload();
          }, 1000);
        }
      }, "json");
    }
  });
});
</script>
