<?php
$this->load->model('mpost');
$ruser = GetLoggedUser();
$docs = $this->mpost->search(0,"",3);
 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $rpengguna = $this->db->where(COL_ROLEID, ROLEOPERATOR)->count_all_results(TBL__USERS);
        $rskpd = $this->db->count_all_results(TBL_MSKPD);
        $rdoc = $this->db->count_all_results(TBL_TDOCS);
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3><?=number_format($rpengguna)?></h3>

              <p class="font-italic">PENGGUNA</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($rskpd)?></h3>

              <p class="font-italic">SKPD</p>
            </div>
            <div class="icon">
              <i class="fas fa-institution"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($rdoc)?></h3>

              <p class="font-italic">DOKUMEN</p>
            </div>
            <div class="icon">
              <i class="fas fa-book"></i>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="col-lg-12">
        <div class="card card-secondary collapsed-card">
          <div class="card-header">
            <h5 class="card-title">PETUNJUK PENGGUNAAN</h5>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
            </div>
          </div>
          <div class="card-body p-0">
            <iframe src="<?=MY_IMAGEURL.'doc-userguide.pdf'?>" width="100%" height="500px"></iframe>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card card-olive">
          <div class="card-header">
            <h5 class="card-title">AKTIVITAS TERKINI</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered table-hover table-condensed" style="margin-top: 0 !important">
              <thead>
                <tr>
                  <th style="white-space: nowrap">TANGGAL / WAKTU</th>
                  <th>DOKUMEN / SKPD</th>
                  <th>KETERANGAN</th>
                  <!--<th>OLEH</th>-->
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if($ruser[COL_ROLEID]!=ROLEADMIN) {
                  $this->db->where(TBL_TDOCS.'.'.COL_IDSKPD, $ruser[COL_COMPANYID]);
                }

                $rlogs = $this->db
                ->select("MAX(LogTimestamp) as LogTimestamp, MAX(tdoclogs.Uniq) as Uniq")
                ->join(TBL_TDOCS,TBL_TDOCS.'.'.COL_DOCID." = ".TBL_TDOCLOGS.".".COL_DOCID,"left")
                ->group_by(TBL_TDOCLOGS.'.'.COL_DOCID)
                ->order_by('MAX(LogTimestamp)', 'desc')
                ->limit(10)
                ->get(TBL_TDOCLOGS)
                ->result_array();

                if(!empty($rlogs)) {
                  foreach($rlogs as $r) {
                    $rlog_ = $this->db
                    ->select("tdoclogs.*, tdocs.*, mskpd.*, COALESCE(_userinformation.Name, tdoclogs.LogBy) as LogName")
                    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TDOCLOGS.".".COL_LOGBY,"left")
                    ->join(TBL_TDOCS,TBL_TDOCS.'.'.COL_DOCID." = ".TBL_TDOCLOGS.".".COL_DOCID,"left")
                    ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TDOCS.".".COL_IDSKPD,"left")
                    ->where(TBL_TDOCLOGS.'.'.COL_UNIQ, $r[COL_UNIQ])
                    ->get(TBL_TDOCLOGS)
                    ->row_array();
                    if(empty($rlog_)) {
                      continue;
                    }

                    $desc_ = $rlog_[COL_LOGREMARKS];
                    $files_ = "";

                    if(!empty($rlog_[COL_LOGFILE])) {
                      $arrfile = explode(",", $rlog_[COL_LOGFILE]);
                      foreach($arrfile as $f) {
                        if(file_exists(MY_UPLOADPATH.$f)) $files_ .= '<a href="'.(MY_UPLOADURL.$f).'" target="_blank"><i class="far fa-link pr-2"></i></a>';
                      }
                    }

                    ?>
                    <tr>
                      <td style="width: 50px; white-space: nowrap; vertical-align: middle"><?=date('d-m-Y', strtotime($rlog_[COL_LOGTIMESTAMP]))?>&nbsp;<sup class="font-italic"><?=date('H:i', strtotime($rlog_[COL_LOGTIMESTAMP]))?></sup></td>
                      <td style="width: 300px; white-space: nowrap; vertical-align: middle"><?=$rlog_[COL_DOCNAME]?><br /><small class="font-italic"><?=$rlog_[COL_SKPDNAMA]?></small></td>
                      <td style="vertical-align: middle"><?=$desc_?><?=!empty($rlog_)&&!empty($rlog_[COL_LOGFILE])?'<span class="pull-right">'.$files_.'</span>':''?></td>
                      <!--<td style="width: 50px; max-width:250px; overflow:hidden !important; text-overflow:ellipsis; white-space:nowrap; font-style: italic; vertical-align: middle"><?=!empty($rlog_)?$rlog_['LogName']:''?></td>-->
                      <td style="width: 50px; white-space: nowrap; vertical-align: middle">
                        <a href="<?=site_url('site/doc/view/'.$rlog_[COL_DOCID])?>" class="btn btn-xs btn-primary"><i class="far fa-search"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="6" class="text-center font-italic text-sm">KOSONG</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card card-olive">
          <div class="card-header">
            <h5 class="card-title">INFORMASI</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <tbody>
                <?php
                if(!empty($docs)) {
                  foreach($docs as $d) {
                    $rfile = $this->db
                    ->where(COL_POSTID, $d[COL_POSTID])
                    ->get(TBL__POSTIMAGES)
                    ->result_array();
                    ?>
                    <tr>
                      <td><a target="_blank" href="<?=!empty($rfile)?MY_UPLOADURL.$rfile[0][COL_IMGPATH]:''?>"><?=$d[COL_POSTTITLE]?></a></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
</script>
