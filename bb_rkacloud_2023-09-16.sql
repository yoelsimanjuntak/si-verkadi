# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: bb_rkacloud
# Generation Time: 2023-09-16 10:07:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_homepage` WRITE;
/*!40000 ALTER TABLE `_homepage` DISABLE KEYS */;

INSERT INTO `_homepage` (`Uniq`, `ContentID`, `ContentTitle`, `ContentType`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'TxtWelcome1','Sambutan & Foto','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','k_dinkes2.png',NULL,NULL),
	(2,'TxtWelcome2','Struktur Organisasi','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','STRUKTURORGANISASI.jpeg',NULL,NULL),
	(41,'NumProfile','NUMBER 1','NUMBER','Angka 1',NULL,'1','Satuan'),
	(42,'NumProfile','NUMBER 2','NUMBER','Angka 2',NULL,'2','Satuan'),
	(43,'NumProfile','NUMBER 3','NUMBER','Angka 3',NULL,'3','Satuan'),
	(44,'NumProfile','NUMBER 4','NUMBER','Angka 4',NULL,'4','Satuan'),
	(45,'TxtPopup1','Tugas Pokok dan Fungsi','PDF',NULL,'rekomsaranakesehatan.pdf',NULL,NULL),
	(46,'TxtPopup1','Maklumat Pelayanan','IMG',NULL,'rekomsaranakesehatan1.pdf',NULL,NULL),
	(47,'TxtPopup1','Standar Pelayanan','PDF',NULL,'rekomsaranakesehatan2.pdf',NULL,NULL),
	(49,'TxtPopup2','Kesehatan Masyarakat','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(50,'TxtPopup2','Pencegahan & Pengendalian Penyakit','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(51,'TxtPopup2','Pelayanan & Sumber Daya Kesehatan','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(52,'TxtPopup3','Instalasi Farmasi','DB','select * from munit where UnitTipe = \'FARMASI\' order by UnitNama','UnitNama','Uniq',NULL),
	(53,'TxtPopup3','Puskesmas','DB','select * from munit where UnitTipe = \'PUSKESMAS\' order by UnitNama','UnitNama','Uniq',NULL),
	(55,'Carousel','Carousel 1','IMG',NULL,NULL,NULL,NULL),
	(56,'Carousel','Carousel 2','IMG',NULL,NULL,NULL,NULL),
	(57,'Carousel','Carousel 3','IMG',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','VERKADI'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','VERIFIKASI RKA DIGITAL'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Badan Keuangan dan Aset Daerah'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KABUPATEN BATU BARA');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(11) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(11) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(11) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	(1,'admin','administrator.rkacloud@bkad.batubarakab.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-11-19'),
	(3,'developer','rkacloud.developer@bkad.batubarakab.go.id',NULL,'Developer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-11-22'),
	(4,'bkpsdm','rkacloud.bkpsdm@bkad.batubarakab.go.id','29','BKPSDM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-11-22'),
	(5,'bkad','rkacloud.bkad@bkad.batubarakab.go.id','28','BKAD',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-12-12');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','2cb959857bd85a58cbd0642e78231508',1,0,'2022-12-21 11:11:22','::1'),
	(3,'developer','e10adc3949ba59abbe56e057f20f883e',1,0,NULL,NULL),
	(4,'bkpsdm','e10adc3949ba59abbe56e057f20f883e',2,0,'2022-12-03 10:06:13','::1'),
	(5,'bkad','e6c8b9a09b4a0a7149ec817982c61a77',2,0,'2022-12-12 21:45:14','127.0.0.1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mskpd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mskpd`;

CREATE TABLE `mskpd` (
  `SkpdId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SkpdNama` text,
  `SkpdNamaPimpinan` varchar(200) DEFAULT NULL,
  `SkpdNamaJabatan` varchar(200) DEFAULT NULL,
  `SkpdKop` text,
  `SkpdIsAktif` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(200) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(200) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SkpdId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `mskpd` WRITE;
/*!40000 ALTER TABLE `mskpd` DISABLE KEYS */;

INSERT INTO `mskpd` (`SkpdId`, `SkpdNama`, `SkpdNamaPimpinan`, `SkpdNamaJabatan`, `SkpdKop`, `SkpdIsAktif`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'DINAS PENDIDIKAN','ILYAS SITORUS, M.Pd','KEPALA DINAS PENDIDIKAN','Jln.Besar Perupuk Dusun v Desa Perupuk Kecamatan Lima Puluh Pesisir',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(2,'DINAS KESEHATAN, PENGENDALIAN PENDUDUK DAN KELUARGA BERENCANA ','drg. Wahid Khusyairi M.M','Kepala Dinas kesehatan, Pengendalian Penduduk dan Keluarga Berencana','Jl. Perintis Kemerdekaan No 49, Lima Puluh',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(3,'DINAS PEKERJAAN UMUM DAN TATA RUANG','Ir. KHAIRUL ANWAR, M. Si','KEPALA DINAS PEKERJAAN UMUM DAN TATA RUANG','Jln. Lintas Gambus Laut Desa Gambus Laut Kecamatan Lima Puluh Pesisir Kabupaten Batu Bara\nKode Pos 21255',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(4,'DINAS PERUMAHAN, KAWASAN PERMUKIMAN DAN LINGKUNGAN HIDUP','NORMA DELI SIREGAR, SE. MM','KEPALA DINAS PERUMAHAN, KAWASAN PERMUKIMAN DAN LINGKUNGAN HIDUP','Jalan.  Besar Simpang Dolok, Desa Simpang Dolok Kecamatan Datuk Lima Puluh - 21255',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(5,'BADAN PENANGGULANGAN BENCANA DAERAH','MUHAMMAD SABAN EFENDI HARAHAP, SE','Kepala Badan Penanggulangan Bencana Daerah','Jln. Lintas  Desa Perupuk Kecamatan Lima Puluh Pesisir',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(6,'SATUAN POLISI PAMONG PRAJA','Drs. Abdul Rahman Hadi','Kepala Satuan Polisi Pamong Praja','Jl. Inpres LK. II Limapuluh Kota',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(7,'BADAN KESATUAN BANGSA DAN POLITIK','AZWAR, SP','KEPALA BADAN','Jln. Besar Lima Puluh Simpang Dolok Km. 8 Pulau Sejuk\nKecamatan Datuk Lima Puluh 21255',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(8,'DINAS SOSIAL, PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK','RIYADI, S.Pd, M.Pd','KEPALA DINAS ','JL. BESAR PERUPUK, DUSUN V, DESA PERUPUK, KEC. LIMA PULUH PESISIR',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(9,'DINAS KETENAGAKERJAAN, PERINDUSTRIAN DAN PERDAGANGAN','BUHARI IMRAN, S.S, M.Si','KEPALA DINAS','JLN. IMAM BONJOL NO. 11 LABUHAN RUKU KEC. TALAWI ',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(11,'DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL','MAEDA SOETOPO,S.STP.,MSP','Kepala Dinas Kependudukan dan Pencatatan Sipil','Jl. Lintas Sumatera Desa tanah Merah Kec. Air Putih Kab. Batu bara',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(12,'DINAS PEMBERDAYAAN MASYARAKAT DAN DESA','RADYANSYAH F. LUBIS, S.Sos','KEPALA DINAS','JL. JENDERAL SUDIRMAN DUSUN ASOKA DESA SIPARE-PARE KECAMATAN AIR PUTIH',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(14,'DINAS PERHUBUNGAN','JONNIS MARPAUNG, S.Pd','KEPALA DINAS','JALAN LINTAS SUMATERA DUSUN I DESA KARANG BARU KECAMATAN DATUK TANAH DATAR KEBUPATEN BATU BARA',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(15,'DINAS KOMUNIKASI DAN INFORMATIKA','EDWIN ALDRIN SITORUS, S.Sos, S.E., M.Si','KEPALA DINAS','Jl. Jenderal Sudirman No. 77A Kel. Indrasakti, Kec. Air Putih Kode Pos 21256. Email: diskominfo@batubarakab.go.id. Website: www.diskominfo.batubarakab.go.id\n\n',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(16,'DINAS KOPERASI DAN USAHA KECIL DAN MENENGAH','ARIF HANAFIAH, S.STP','KEPALA DINAS','JL. JEND SUDIRMAN,  KEL. INDRASAKTI, KEC. AIR PUTIH, KABUPATEN BATUBARA',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(17,'DINAS PENANAMAN MODAL DAN PELAYANAN PERIZINAN TERPADU SATU PINTU','ROSDIANA DAMANIK, SE. MM','KEPALA DINAS','Jln. Perintis Kemerdekaan No. 54 A Kec. Lima Puluh Kota Kab. Batu Bara',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(18,'DINAS PEMUDA, OLAHRAGA, KEBUDAYAAN DAN PARIWISATA','Drs. SAPRI, MM','Kepala Dinas','Jln. Besar Perupuk Desa Perupuk Kecamatan Lima Puluh Pesisir - 21655',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(19,'DINAS PERPUSTAKAAN','Elpandi, S.Ag','Plt. Kepala Dinas Perpustakaan','Jalan Protokol Dusun IV Desa Pahang Kecamatan Talawi Kode Pos 21254',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(20,'DINAS PERIKANAN DAN PETERNAKAN','ANTONI RITONGA, SP.','KEPALA','Jalan Lintas Perupuk Desa Perupuk, Kec. Lima Puluh Pesisir ',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(21,'DINAS PERTANIAN DAN PERKEBUNAN','MUHAMMAD RIDWAN, SP., M.Agric, SC','KEPALA DINAS PERTANIAN DAN PERKEBUNAN','Jl. H. Barus Siregar Desa Tanjung Mulia Kecamatan Air Putih 21256',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(24,'SEKRETARIAT DAERAH KABUPATEN','H. Sakti Alam Siregar, SH','Sekretaris Daerah','Jln. PERINTIS KEMERDEKAAN NO. 164  TELP. (0622) - 96782 LIMA PULUH ? 21255',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(25,'SEKRETARIAT DPRD','AGUS ANDIKA.S.Sos.,M.Si','SEKRETARIS DPRD ','Jalan  Perintis Kemerdekaan  NO.53 Kecamatan Lima Puluh',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(26,'BADAN PERENCANAAN PEMBANGUNAN DAN PENELITIAN PENGEMBANGAN DAERAH','H. ABDU ZAHRUL, S.Sos, M.Si','KEPALA BADAN','Jalan Lintas Desa Simpang Dolok Kecamatan Datuk Lima Puluh - 21225',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(27,'BADAN PENDAPATAN DAERAH','RIJALI S.Pd','KEPALA BADAN PENDAPATAN DAERAH','Jl. Lintas Sumatera KM.110 Desa Pematang Panjang Kecamtan Air Putih Kabupten Batu Bara',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(28,'BADAN KEUANGAN DAN ASET DAERAH','Dr. Ir. H. Hakim, M.Si.','Kepala Badan Keuangan dan Aset Daerah','Jalan Lintas Sumatera Utara KM 110. Kecamatan Air Putih Kabupaten Batu Bara',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(29,'BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA','MHD. DAUD, S.Pd, SH, MM','KEPALA BADAN','Jl. Imam Bonjol Kel. Labuhan Ruku Kec. Talawi',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(30,'INSPEKTORAT DAERAH','H.ATTARUDDIN, S.Pd., M.M','Inspektur','Mangkai Baru Kec.Lima Puluh Batu Bara Kode Pos 212555',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(31,'KECAMATAN LIMA PULUH','ADRI AULIA HARAHAP, S.STP','CAMAT LIMA PULUH','Jalan : Pelajar Kelurahan Lima Puluh Kota Kec. LIma Puluh',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(32,'KECAMATAN TALAWI','EFENDI,S.T','CAMAT TALAWI','Jl.Imam Bonjol No.113 Labuhan Ruku Kode Pos 21254',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(33,'KECAMATAN TANJUNG TIRAM','JUNAIDI, SH','CAMAT','JL. PENDIDIKAN DUSUN XI DESA SUKA MAJU KECAMATAN TANJUNG TIRAM',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(34,'KECAMATAN AIR PUTIH','RAHMAD KHAIDIR LUBIS, S.STP. M.AP.','CAMAT AIR PUTIH','Jln Jendral Sudirman No.77 Indrapura Kecamatan Air Putih Kabupaten Batu Bara',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(35,'KECAMATAN SEI SUKA','TUMPAK SARAGI, S.SOS,M.Si','CAMAT','Jl. Access Road Inalum No.1 Perk. Sipare Pare ',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(36,'KECAMATAN MEDANG DERAS','SYAHRIZAL, SH','CAMAT MEDANG DERAS','JLN. JENDERAL SUDIRMAN KELURAHAN PANGAKALAN DODEK BARU',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(37,'KECAMATAN SEI BALAI','WALI WALA AZAHARI SAGALA, S.Pd','CAMAT SEI BALAI','JLN. SOEJONO HUMARDANI (JALINSUM) DESA PERJUANGAN KODE POS 21252',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(38,'KECAMATAN LIMA PULUH PESISIR','ROZALI, S.Pd, M.Pd','CAMAT LIMA PULUH PESISIR','Jl. Besar Desa Perupuk, Kec Lima Puluh Pesisir, Kab. Batu Bara Kode Pos 21255',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(39,'KECAMATAN DATUK LIMA PULUH','ZAMZAMY ELWADIIP, S.IP, M,Si','CAMAT DATUK LIMA PULUH','JL. Besar Lima Puluh Simp. Dolok KM. 8 Pulau Sejuk No. 7',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(40,'KECAMATAN DATUK TANAH DATAR','MULIADI, S.E','CAMAT DATUK TANAH DATAR','Jln.Lintas Sumatera Desa Karang Baru Kode Pos 21254',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(41,'KECAMATAN NIBUNG HANGUS','SUKANDAR. S.Sos','CAMAT NIBUNG HANGUS','Jln. Manunggal V  No 100 Ujung Kubu Kota Kode Pos 21253',1,'admin','2021-11-26 12:12:12',NULL,NULL),
	(42,'KECAMATAN LAUT TADOR','WANDI, S.E.','CAMAT LAUT TADOR','Jalan Arjo Utomo Dusun I Kelapa Desa Pelanggiran Laut Tador',1,'admin','2021-11-26 12:12:12',NULL,NULL);

/*!40000 ALTER TABLE `mskpd` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tdoclogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdoclogs`;

CREATE TABLE `tdoclogs` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DocID` bigint(10) unsigned NOT NULL,
  `LogRemarks` text,
  `LogTimestamp` datetime DEFAULT NULL,
  `LogFile` varchar(200) DEFAULT NULL,
  `LogBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_LOG_DOC` (`DocID`),
  CONSTRAINT `FK_LOG_DOC` FOREIGN KEY (`DocID`) REFERENCES `tdocs` (`DocId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tdoclogs` WRITE;
/*!40000 ALTER TABLE `tdoclogs` DISABLE KEYS */;

INSERT INTO `tdoclogs` (`Uniq`, `DocID`, `LogRemarks`, `LogTimestamp`, `LogFile`, `LogBy`)
VALUES
	(1,1,'Dokumen diterima.','2022-12-03 08:56:12','61G62PA.pdf','admin'),
	(2,1,'Status diperbarui.','2022-12-03 09:21:11',NULL,'admin');

/*!40000 ALTER TABLE `tdoclogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tdocs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdocs`;

CREATE TABLE `tdocs` (
  `IdSkpd` bigint(20) unsigned NOT NULL,
  `DocID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DocName` text NOT NULL,
  `DocRemarks` text,
  `DocNomor` varchar(200) DEFAULT NULL,
  `DocTahun` int(11) DEFAULT NULL,
  `DocURL` text NOT NULL,
  `DocType` enum('rka','lainnya') DEFAULT NULL,
  `DocStatus` enum('SUBMITTED','VERIFIED') DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `VerifiedBy` varchar(50) DEFAULT NULL,
  `VerifiedOn` datetime DEFAULT NULL,
  `IsVerifKesesuaian` tinyint(1) NOT NULL DEFAULT '0',
  `IsVerifKode` tinyint(1) NOT NULL DEFAULT '0',
  `IsVerifKelengkapan` tinyint(1) NOT NULL DEFAULT '0',
  `VerifiedRemarks` text,
  PRIMARY KEY (`DocID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `tdocs` WRITE;
/*!40000 ALTER TABLE `tdocs` DISABLE KEYS */;

INSERT INTO `tdocs` (`IdSkpd`, `DocID`, `DocName`, `DocRemarks`, `DocNomor`, `DocTahun`, `DocURL`, `DocType`, `DocStatus`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `VerifiedBy`, `VerifiedOn`, `IsVerifKesesuaian`, `IsVerifKode`, `IsVerifKelengkapan`, `VerifiedRemarks`)
VALUES
	(28,1,'TEST','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','094/1212/BKAD/2022',2022,'61G62PA.pdf','rka','SUBMITTED','admin','2022-12-03 08:56:12','admin','2022-12-03 09:31:43',NULL,NULL,1,0,1,'Testing..');

/*!40000 ALTER TABLE `tdocs` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
